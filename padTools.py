from datetime import datetime, timezone, timedelta
import json
import re
import time
from typing import Callable
from urllib.parse import urlparse

import requests
from slugify import slugify

def getPadAllInfo(issue, padUrl:str, padRootUrl:str, padMaxAge:timedelta, maxRevisions:int, dryrun:bool, log:Callable[[str],str]=lambda s:s):
    addCommentToIssue = False
    try:
        padUpdateTime, padTitle, padInfoParsed = _getPadInfoAndUpdateTime(padUrl)
    except Exception as e:
        log(f'E Could not get info or body for pad {padUrl}: {e}. Skipping.')
        raise Exception()

    log(f'V Pad `{padTitle}` was last updated on {padUpdateTime}.')
    if padUpdateTime < datetime.now(timezone.utc) - padMaxAge:
        if padUrl == padRootUrl:
            log(f'I  Root pad has not been modified for longer than {padMaxAge}. Closing Issue. Skipping.')
            addCommentToIssue = True
            issue.add_labels = "expired"
            issue.state_event = 'close'
            if not dryrun:
                issue.save()
        else:
            log(f'V  Pad has not been modified for longer than {padMaxAge}. Skipping.')
        raise Exception()

    try:
        padFullInfo, padAuthors = _getPadFullInfoAndAuthors(padUrl)
    except Exception as e:
        log(f'E Could not get author infos for pad {padUrl.geturl()}: {e}. Skipping.')
        raise Exception()

    try:
        padRevisionsParsed = _getPadRevisions(padUrl, padFullInfo, padAuthors, maxRevisions)
    except Exception as e:
        log(f'E Could not get revisions for pad {padUrl.geturl()}: {e}. Skipping.')
        raise Exception()

    # if padRevisionsParsed[0]['details']['content'] == '':
    #     padRevisionsParsed.pop(0)

    return padTitle, padInfoParsed, padRevisionsParsed, padAuthors, addCommentToIssue

def _getPadFullInfoAndAuthors(padUrl):
    padMeUrl = urlparse(f'{padUrl.scheme}://{padUrl.netloc}/me')
    padCookies = requests.get(f'{padMeUrl.geturl()}').cookies
    padNoteId = padUrl.path[1:].split('/',1)[0]
    padSocketUrl = urlparse(f'{padUrl.scheme}://{padUrl.netloc}/socket.io/?noteId={padNoteId}&t={datetime.now().timestamp()}&EIO=4&transport=polling')
    padSid = json.loads(re.findall(r'[0-9:]+(.*)', requests.get(f'{padSocketUrl.geturl()}', cookies=padCookies).text)[0])['sid']
    padFullInfoRaw = requests.get(f'{padSocketUrl.geturl()}&sid={padSid}', cookies=padCookies).text
    padFullInfo = json.loads(re.findall(r'[0-9:]+(\["refresh",.*?\])(?:[0-9]|$)', padFullInfoRaw)[0])[1]
    padAuthors = padFullInfo['authors']
    return padFullInfo, padAuthors

def _getPadInfoAndUpdateTime(padUrl):
    padInfo = requests.get(f'{padUrl.geturl()}/info').json()
    padUpdateTime = datetime.fromisoformat(padInfo["updatetime"])
    padTitle = padInfo["title"]
        #padBody = requests.get(f'{padUrl.geturl()}/download').text
    padInfoParsed = {}
    padInfoParsed['id'] = padUrl.path[1:].split('/',1)[0]
    for k, v in padInfo.items():
        padInfoParsed[k] = v
    for k in ['createtime', 'updatetime']:
        padInfoParsed[k] = datetime.fromisoformat(padInfoParsed[k])
    for k in ['title']:
        padInfoParsed[f'{k}_slug'] = slugify(padInfoParsed[k])
    return padUpdateTime, padTitle, padInfoParsed

def _getPadRevisions(padUrl, padFullInfo, padAuthors, maxRevisions):
    padRevisions = requests.get(f'{padUrl.geturl()}/revision').json()['revision']
    padRevisionsParsed = []
    revisionIdx = 0
    for r in padRevisions:
        rp = {}
        rp['ownerprofile'] = padFullInfo['ownerprofile']
        rp['owner'] = padFullInfo['owner']
        for k, v in r.items():
            rp[k] = v
        for k in ['time']:
            rp[f'{k}_raw'] = r[k]
            rp[k] = datetime.fromtimestamp(r[k]/1000, timezone.utc)
        padRevisionsParsed.append(rp)
    padRevisionsParsed.reverse()
    return padRevisionsParsed

def getPadRevisionDetails(padUrl, padRevision, padAuthors, log:Callable[[str],str]=lambda s:s):
    rp = {}
    for i in range(1,6) :
        try:
            rp['details'] = requests.get(f'{padUrl.geturl()}/revision/{padRevision["time_raw"]}').json()
            break
        except Exception as e:
            log(f'V Error getting revision details from pad. Retrying {i}/5.')
            time.sleep(i)
    
    if not 'details' in rp:
        raise Exception()

    if rp['details']['authorship']:
        rp['author'] = padAuthors[rp['details']['authorship'][0][0]]
    else:
        rp['author'] = padRevision['ownerprofile'] or {'userid': '00000000-0000-0000-0000-000000000000', 'name': 'Anonymous', 'photo': 'https://cdn.libravatar.org/avatar/40f8d096a3777232204cb3f796c577b7?default=identicon&s=400'}
        rp['author']['userid'] = padRevision['owner']
    return rp