# Hello I'm Padbot 🤖

I am a little helper that copies pads from Hedgedoc to Gitlab. 

Every revision of a pad will be a commit to a given file in the repository.

## How To Use

I will look at the open [issues](https://git.rwth-aachen.de/bots/padbot/-/issues) in this project every couple of minutes.

To let me know what to do add at least one `yaml` code block in your issue like so:

````
```yaml
padUrl: https://pad.otc.coscine.dev/a1QYFtx5TECJUmZlIqOBmA?both
gitUrl: https://git.rwth-aachen.de/spielwiese/ci-test/-/blob/master/afolder/{info[title_slug]}.md
```
````

Note that only `padUrl` is required. It is used to specify the pad that I should copy to Gitlab. See discussion on [pad prerequisites](#pad-prerequisites) below to make sure I can read your pad. I was tested on `https://pad.otc.coscine.dev/` but other Hedgedoc servers should work, too.

The parameter `gitUrl` is used to specify where your pad goes. You can also specify it in the frontmatter of the pad. If it is neither in the frontmatter of the pad nor in the issue, I will close your issue 😤. Use the URL as displayed by Gitlab in the adress bar of the webbrowser.

I need access to your project to make the commit (duh!). You can provide an access token (see [other parameters](#more-parameters) below, not recommended!) or give me access to your project (recommended). To give me access, invite the group `bots/padbots` to your project or group and make sure they can push to the given branch (beware of protected branches!). Alternatively you can fork me and set up your own runner.

I can interpret certain placeholders in `gitUrl`. This is useful if you want to save pad revisions in individual files rather than git commits. See section on [placeholders](#placeholders) below.

If you want, you can add more text to your issue description as long as all `yaml` code blocks are valid. I will not care about the rest.

In case something goes horribly wrong, I might close your issue, tag you and not look at it again 🙈 until you re-open it - best fix the problem first.

If the initially linked pad has not been changed for `90` days, I will get bored of your pad 🥱 and will also close the issue.

### More Parameters

You may add some additional parameters to control my behaviour:

````
```yaml
dryrun: False
maxdepth: 5
logLevel: verbose
gitlabToken: glpat-abc123def456ghi789jk
```
````

These paramters will fall back to default values:

* `dryrun`: I will only look at the pad but not do any commits. Default: from environment variable `PADBOT_DRYRUN` or `true` if nothing is set.
* `maxdepth`: Number of pad links that I will follow. Values higher than the default will not be respected. Default: from environment variable `PADBOT_MAXDEPTH` or 5 if not set.
* `logLevel`: Verbosity of my output, either `verbose`, `info`, `error`. I will add a log to your issue. Default: from environment variable `PADBOT_LOGLEVEL` or `info` if not set.
* `gitlabToken`: PAT used for the commit. It may not be a good idea to put this in an issue. See discussion on [access tokens](#access-tokens) below. Default: My own token.

### Placeholders

Within `gitUrl` you can use parameters and some basic formatting. Values should be encapsulated in curly braces `{...}`. Generally I know the following things:

* `info` - General pad info as dictionary
  * `info[id]` - Id of the note on the server
  * `info[title]` - Title of the pad as extracted by Hedgedoc
  * `info[title_slug]` - Slugified version of the title
  * `info[createtime]` - Time of pad creation
  * `info[updatetime]` - Time of last update
* `url` - Pad URL as array
  * `url[0]` - Scheme (likely always `https`)
  * `url[1]` - Host (e.g. `pad.otc.coscine.dev`)
  * `url[2]` - Path (e.g. `/a1QYFtx5TECJUmZlIqOBmA`)
* `revision` - general revision info as dictionary
  * `revision[time]` - Time of revision creation
  * `revision[author][name]` - Displayname of the person editing the revision, can be `Anonymous`.
* `frontmatter` - Markdown frontmatter of the pad revision as dictionary

For all times you can specify a format string like [`{revision[time]:%Y-%m-%d}`](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes) to access only portions of the date. Other [formatting options](https://docs.python.org/3/library/string.html#format-string-syntax) may also work (mostly untested).

In verbose logging mode I will comment a more detailed list of possible values to your issue. Combine with `dryrun: true` to check if I can really understands your placeholders in `gitUrl`. Especially with your own frontmatter. Use other key, not listed above, with caution as some may not be set under all conditions.

### Access Tokens

If you need to provide an access token within the issue you should be aware that this may be visible to others in the issue description. This is needed if your repository lives on a different Gitlab server as my access token is definetely not valid there.

I will make issues confidential to (at least a little bit) reduce the risk of information leak (also so others cannot read your pad URLs) - better would be if you make that before that anyway.

The access token will need at least `api` access with the `developer` role.

Note: posting the access token in the issue will essentially allow everyone who can read the issue to copy your token. This is not very secure!

### Pad Prerequisites

Pads need to be visible by guests (`🍃 Freely`, `🛡️ Editable`, `🔒 Locked`). With other modes (`🪪 Limited`, `☂️ Protected` or `✋ Private`) I will not be able to access the pad.

### Caveats, Quirks, Hidden Features, and Known Issues

#### Project Minimal Access Restriction

In order to create commits I will need access to the project in Gitlab - likely at least `Developer`.

When using my default access token, I will try to avoid situations where an issue author ("accidentally" 🦹) instructs me to create commits in a project that they do not have access to themselves. Therefore when creating an issue you need to have at least the same role in the projects referenced by `gitUrls` as I have. 

#### Linked Pads

I will follow linked pads on the same server as the initial pad and try to commit it to a given `gitUrl` - but only if they are real markdown links using the `[...](...)` syntax. You should use [placeholders](#placeholders) or make sure to have different `gitUrl`s in all frontmatters of the pads otherwise I might [refuse to copy](#duplicate-pad-and-git-urls) the pad.

In the committed Markdown all occurences of the linked pad will be replaced by a relative path. Thus links should reference to the right file in Gitlab. Linking across Gitlab projects might not work.

#### Linked Images

I will also copy linked images in the `![](...)` from the same server to the git repository. I will retain the same name, likely a GUID, and commit the image to a `uploads` folder right next to the current pads' file. An existing image in the git repository will never be updated - assuming a new upload will gat a new file name. Likewise I will not delete an image if it is no longer referenced by the pad - so expect some zombies 🧟.

#### Issue Edit Event

If you edit an issue Gitlab will raise an event so I will run immediatly on this issue (only) and not wait for the next schedule so you do not have to wait 🚄. This may be slightly inconsistent when iterating over all issues during the next schedule if I see a `padUrl` or `gitUrl` twice (see discussion on [duplicate URLs](#duplicate-pad-and-git-urls) below).

#### Branches

You may specify other branches in the `gitUrl` (also using placeholders if you are a little crazy ...). I will attempt to create the branch from the default branch if it does not exist (currently mostly untested).

If branches contain a `/`, you should create the branch before I run, since I will otherwise mistake everything after the first `/` as a folder within the repository. This is because within the Gitlab URL the branch and folder are otherwise indistinctive. 

#### Commit Metadata and Times

I will use the username from the pad together with an artificially created email adress in the form `{userid}@{padserver}` in the commit metadata. 

Unfortunately the [Gitlab API](https://docs.gitlab.com/ee/api/commits.html) does not allow to specify the time of a commit (only `author_name` and `author_email`). Revisions are therefore always committed with the current timestamp. Especially when copying "old" pads this can be significantly off but I will add revisuin times to the commit message.

#### Revisions

Hedgedoc will only create a revision about every 10 minutes. It may take some time to pick up the latest changes if Hedgedoc did not yet update the latest revision. Especially in a busy pad edits from multiple people will be squashed into one revision and appear to be authored by only one of the users.

#### Duplicate Pad and Git URLs

I am a bit lazy (or efficient) 😅. During the schedule I will process issues by issue id in ascending order. If I encounter a pad twice I will not process it again but ask you to resolve the conflict with the author (which may be hard for you because the issues are confidential 😈). 

Also one file in a git repository will only be updated from a single source pad to avoid conflicts. However, if you use [placeholders](#placeholders), e.g. with the revision time or update time, I might update multiple files from one pad (but only from one).

#### Existing Files and Conflicting Changes

If a file already exists in Gitlab I will not add revisions that are older than the most recent commit. If someone updates the file in in Gitlab all revisions before that will never be committed. Also I will simply commit the next newer revision over the last edit in Gitlab. Nothing is lost - the git history has you covered - but changes may be hidden in there. I will definetely not try a two way sync 😜 so better only edit the pad and not the file in Gitlab.

In the extremeley rare case that a pads' revision is created at the same instant as crating a commit for the previous revision, the latest revision may not be picked up.