import posixpath
from string import Formatter
from urllib.parse import urlsplit, urlunsplit

class _SafeFormatter(Formatter):
    def get_field(self, field_name, args, kwargs):
        if '[_' in field_name or '.' in field_name:
            raise Exception('Invalid format string.')
        return super().get_field(field_name, args, kwargs)

def formatTemplate(template:str, *args , **kwargs) -> str:
    return _SafeFormatter().format(template, *args, **kwargs)

def relativeUrl(destination, source):
    u_dest = urlsplit(destination)
    u_src = urlsplit(source)

    _uc1 = urlunsplit(u_dest[:2]+tuple('' for i in range(3)))
    _uc2 = urlunsplit(u_src[:2]+tuple('' for i in range(3)))

    if _uc1 != _uc2:
        return destination

    _relpath = posixpath.relpath(u_dest.path, posixpath.dirname(u_src.path))

    return urlunsplit(('', '', _relpath, u_dest.query, u_dest.fragment))