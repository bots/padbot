import gitlab
import os
import json
import frontmatter
from urllib.parse import urlparse, ParseResult

from datetime import datetime, MINYEAR, timezone

from markdownTools import *
from gitlabTools import *
from stringTools import *
from padTools import *

global_dryrun = os.environ['PADBOT_DRYRUN'].lower()=='true' if 'PADBOT_DRYRUN' in os.environ else True
global_maxDepth = int(os.environ['PADBOT_MAXDEPTH']) if 'PADBOT_MAXDEPTH' in os.environ else 5
global_padMaxAge = timedelta(days=int(os.environ['PADBOT_MAXAGE'])) if 'PADBOT_MAXAGE' in os.environ else timedelta(days=90)
global_logLevel = (os.environ['PADBOT_LOGLEVEL'] if 'PADBOT_LOGLEVEL' in os.environ else 'info').upper() # 'verbose' > 'info' > 'error'
global_padMaxRevisions = int(os.environ['PADBOT_MAXREVISIONS']) if 'PADBOT_MAXREVISIONS' in os.environ else 100

issueLogLines = []
def log(s):
    s = re.sub(r'^(\s*)([A-Z])(\s+)', lambda m: f'{m.group(2)}{m.group(3) if len(m.group(3)) > 1 else ""}{m.group(1)} ', s)
    issueLogLines.append(s)
    if s[0] <= global_logLevel[0]: 
        print(s)

log('I Hello! This is Padbot 🤖.')

ciServerUrl = os.environ['CI_SERVER_URL']

ci_gl = gitlab.Gitlab(url=ciServerUrl, private_token=os.environ['GITLAB_ACCESS_TOKEN'])
ci_gl.auth()
ci_project = ci_gl.projects.get(os.environ['CI_PROJECT_ID'])

log(f'I Running on project {ci_project.path_with_namespace} with user {ci_gl.user.username}.')

issueIids=[]
if(os.environ.get('TRIGGER_PAYLOAD')):
    log(f'I Triggered by issue edit event. Single issue mode.')
    f = open(os.environ['TRIGGER_PAYLOAD'])
    payload = json.load(f)
    update_by = payload['user']['username']
    issue_iid = payload['object_attributes']['iid']
    issue_state = payload['object_attributes']['state']
    if issue_state == 'opened':
        issueIids.append(issue_iid)
else:
    log(f'I Running for all issues in project.')
    projectIssues = ci_project.issues.list(state='opened', get_all=True, sort="asc")
    for i in projectIssues:
        issueIids.append(i.iid)

gitHistory = {}
gitPadLastRevisionCommitted = {}

padQueue = []

for iid in issueIids:
    padHistory = {}
    issueLogLines = []

    log(f'I Processing issue #{iid}.')

    issue = ci_project.issues.get(iid)
    log(f'V  Found at {issue.web_url}')
    if issue.state != 'opened':
        log(f'I  Issue is closed. Skipping.')
        continue

    parameters = parseYamlFromMarkdown(issue.description, lambda s : log(f'  {s}'))

    dryrun = parameters['dryrun'] if 'dryrun' in parameters else global_dryrun
    if dryrun:
        log('I  Dry run. Will not save changes.')

    maxdepth = min(abs(parameters['maxdepth']), global_maxDepth) if 'maxdepth' in parameters else global_maxDepth

    ensureIssueIsConfidential(issue, dryrun, lambda s : log(f'  {s}'))

    padRootUrl:ParseResult = urlparse(parameters['padUrl'])._replace(query='', fragment='') if 'padUrl' in parameters else None
    gitUrlRaw = parameters['gitUrl'] if 'gitUrl' in parameters else None
    
    logLevel = parameters['logLevel'].upper() if 'logLevel' in parameters else global_logLevel

    addCommentToIssue = False

    if not padRootUrl:
        log('E  Issue does not define `padUrl`. Closing Issue. Skipping.')
        addCommentToIssue = True
        issue.add_labels = "⚠️ yaml invalid"
        issue.state_event = 'close'
        if not dryrun:
            issue.save()
    else: 
        log(f'V  Found padUrl in issue: {padRootUrl.geturl()}.')

        if padRootUrl.geturl() in padHistory:
            log(f'E    Pad already processed in #{padHistory[padRootUrl.geturl()]}. Skipping.')
            continue
        padHistory[padRootUrl.geturl()] = iid
        padQueue.append(( padRootUrl, issue, 0 ))

    while len(padQueue) > 0:
        padStartTime = datetime.now(timezone.utc)
        padUrl, issue, depth, *_ = padQueue.pop(0)

        log(f'I  Processing pad: {padUrl.geturl()}.')

        try:
            padTitle, padInfoParsed, padRevisionsParsed, padAuthors, addComment = getPadAllInfo(issue, padUrl, padRootUrl, global_padMaxAge, global_padMaxRevisions, dryrun,  lambda s : log(f'    {s}'))
            addCommentToIssue |= addComment
        except:
            continue

        if not padUrl.geturl() in gitPadLastRevisionCommitted:
            gitPadLastRevisionCommitted[padUrl.geturl()] = datetime(MINYEAR, 1, 1, tzinfo=timezone.utc)

        for padRevision in padRevisionsParsed:
            if padRevision['time'] <= gitPadLastRevisionCommitted[padUrl.geturl()]:
                log(f'V      Revision already processed. Skipping.')
                continue

            try:
                padRevisionDetails = getPadRevisionDetails(padUrl, padRevision, padAuthors)
            except:
                log(f'A    Error getting details for revision {padRevision["time"]} of pad {padUrl.geturl()} while committing pad. This is bad some revisions may be lost!')
                continue

            padRevision['details'] = padRevisionDetails['details']
            padRevision['author'] = padRevisionDetails['author']
            padBody = padRevision['details']['content']
            padFrontmatter = frontmatter.loads(padBody).metadata
            padGitUrlRaw = padFrontmatter['gitUrl'] if 'gitUrl' in padFrontmatter else gitUrlRaw

            if not padGitUrlRaw:
                log('E    Could not find `gitUrl` in issue or in pad revision. Skipping.')
                continue
            
            if logLevel >= 'verbose':
                log(f'V    The following placeholders can be used to format the gitUrl:')
                log(f'V      frontmatter: `{json.dumps(padFrontmatter, default=str)}`.')
                log(f'V      info: `{json.dumps(padInfoParsed, default=str)}`.')
                log(f'V      url: `{json.dumps(padUrl, default=str)}`.')
                padRevisionToPrint = dict(padRevision)
                del padRevisionToPrint['details']
                log(f'V      revision: `{json.dumps(padRevisionToPrint, default=str)}`.')

            try:
                gitUrl = formatTemplate(padGitUrlRaw, frontmatter=padFrontmatter, info=padInfoParsed, url=padUrl, revision=padRevision)
                gitUrl = urlparse(gitUrl)
            except Exception as e:
                log(f'E    Could not generate git URL from template: {e}. Skipping.')
                continue
        
            log(f'V    Revision {padRevision["time"]} should be committed to {gitUrl.geturl()}.')

            if gitUrl.geturl() in gitHistory and not gitHistory[gitUrl.geturl()] == padUrl.geturl():
                log(f'E      The git URL {gitUrl.geturl()} clashes with a pad in issue #{padHistory[gitHistory[gitUrl.geturl()]]}. Skipping.')
                continue
            gitHistory[gitUrl.geturl()] = padUrl.geturl()

            try:
                gitProject, gitBranchPath = connectToGitlabProject(issue, parameters, gitUrl, lambda s : log(f'      {s}'))
            except:
                continue
            
            gitBranch, gitStartBranch, gitFilePath = getBranchFromPath(gitBranchPath, gitProject, lambda s : log(f'      {s}'))

            log(f'V      Will push to {gitFilePath} to branch {gitBranch}.')


            padImagesList, padLinkList = getImagesAndLinks(padUrl, padBody)
            log(f'V      Found {len(padLinkList)} linked pads.')

            padLinkQueue = []
            if depth + 1 < maxdepth:
                for padLinkUrl in padLinkList:
                    if not padLinkUrl.geturl() in padHistory:
                        padHistory[padLinkUrl.geturl()] = iid
                        padLinkQueue.append((padLinkUrl, issue, depth + 1))
                        log(f'V      Found new linked padUrl {padLinkUrl.geturl()}.')
            else:
                log(f'I      Not processing more linked pads. Maximum depth of {maxdepth} reached.')

            if len(padLinkQueue) > 0:
                padQueue = padLinkQueue + [ ( padUrl, issue, depth ) ] + padQueue
                log(f'V      Put pad on hold to process linked pads first.')
                break

            gitFilePathExists, gitFileLastUpdate = checkIfFileExistsOnBranch(gitStartBranch or gitBranch, gitFilePath, gitProject)

            if gitFilePathExists:
                log(f'V      File in git repository exists and was updated on {gitFileLastUpdate}.')
            else:
                log('V      File does not exist in git repository yet.')

            if (padRevision['time'] < gitFileLastUpdate) and (gitPadLastRevisionCommitted[padUrl.geturl()] == datetime(MINYEAR, 1, 1, tzinfo=timezone.utc)):
                log(f'V      File in git repository was updated after revision. Skipping.')
                continue

            padBody = makeLinksRelative(padHistory, gitHistory, iid, padBody, gitUrl, padLinkList, lambda s : log(f'        {s}'))

            log(f'V      Found {len(padImagesList)} uploaded images.')
            padBody, gitImgActions = makeImagesRelative(padBody, gitProject, gitStartBranch or gitBranch, gitFilePath, padImagesList, 'uploads', lambda s : log(f'        {s}'))

            commitData = createCommitData(padUrl, padTitle, padRevision, padBody, gitBranch, gitStartBranch, gitFilePath, gitFilePathExists, gitImgActions)
            log(f'V      Will commit {len(commitData["actions"])} files.')

            try:
                if not dryrun:
                    gitProject.commits.create(commitData)
                addCommentToIssue = True
                log(f'I    Revision {padRevision["time"]} committed.')
            except Exception as e:
                log(f'E      Error committing files: {e}.')

            gitPadLastRevisionCommitted[padUrl.geturl()] = padRevision['time']
    
        padEndTime = datetime.now(timezone.utc)
        log(f'V  Processing pad took {padEndTime-padStartTime}.')

        if padEndTime-padStartTime > timedelta(minutes=1):
            log(f'I  Processing pad took too long (likely too many revisions). Enqueueing again.')
            padQueue = padQueue + [ ( padUrl, issue, depth ) ]

    log(f'V Done with issue #{iid}.')

    stash = []
    stashLevel = 'Z'
    for l in issueLogLines:
        if l[0] <= logLevel:
            stash.append(re.sub(r'^([A-Z])(\s*)', lambda m: f'{m.group(2)} * {m.group(1)} ', l))
            stashLevel = min(stashLevel, l[0])
    logMd = '  \n'.join(stash)
    
    if stashLevel <= 'E' or addCommentToIssue:
        message = f'@{issue.author["username"]} something may require your attention.' if stashLevel <= 'E' else ''
        message = f'{message}' if addCommentToIssue else message
        try:
            issue.notes.create({'body': f'{message}\n\nSee Logs below.\n\n<details><summary>Logs</summary>\n\n{logMd}\n</details>'})
        except Exception as e:
            log(f'E Error submitting log to issue: {e}.')

log('I Done with all.')