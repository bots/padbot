from datetime import datetime, timezone, MINYEAR
import os
import re
from typing import Callable
import gitlab

def connectToGitlabProject(issue, parameters, gitUrl, log:Callable[[str],str]=lambda s:s):
    gitServerUrl = f'{gitUrl.scheme}://{gitUrl.netloc}'
    [gitProjectPath, gitBranchPath] = re.split(r'/-/(?:blob|tree)/', gitUrl.path, 1, re.IGNORECASE) #gitUrl.path.split('/-/blob/',1)
    gitProjectPath = gitProjectPath[1:]

    try:
        gitToken = parameters['gitlabToken'] if 'gitLabToken' in parameters else os.environ['GITLAB_ACCESS_TOKEN']
        gl = gitlab.Gitlab(url=gitServerUrl, private_token=gitToken)
        gl.auth()
        log(f'V Connected with user {gl.user.username}.')
    except Exception as e:
        log(f'E Cannot connect to GitLab at {gitUrl.geturl()}: {e}. Skipping.')
        raise Exception()

    #TODO check bot has at least member access
    try:
        gitProject = gl.projects.get(gitProjectPath)
        gitProjectMemberBot = gitProject.members_all.get(gl.user.id)
    except Exception as e:
        log(f'E Could not access project {gitProjectPath} on {gitServerUrl}: {e}. Skipping.')
        raise Exception()
    log(f'V Found GitLab project to push {gitProject.web_url}.')

    if gitToken == os.environ['GITLAB_ACCESS_TOKEN']:
        gitProjectMemberIssueAuthor = None
        try:
            gitProjectMemberIssueAuthor = gitProject.members_all.get(issue.author['id'])
        except Exception as e:
            log(f'E Issue author `{issue.author["name"]}` is not member of project. Skipping.')
            raise Exception()

        if gitProjectMemberBot.access_level > gitProjectMemberIssueAuthor.access_level:
            log(f'E Issue author `{issue.author["name"]}` has insuficient access level, has {gitProjectMemberIssueAuthor.access_level} needs {gitProjectMemberBot.access_level}. Skipping.')
            raise Exception()
    return gitProject, gitBranchPath

def ensureIssueIsConfidential(issue, dryrun:bool=False, log:Callable[[str],str]=lambda s:s):
    if not issue.confidential:
        log(f'I  Issue is not confidential. Setting to confidential.')
        issue.confidential = True
        if not dryrun:
            issue.save()

def getBranchFromPath(gitBranchPath, gitProject, log:Callable[[str],str]=lambda s:s):
    gitProjectBranches = gitProject.branches.list(search=f'^{gitBranchPath.split("/",1)[0]}', get_all=True)
    gitBranch=None
    for b in gitProjectBranches:
        if gitBranchPath.startswith(b.name):
            gitBranch = b.name
            break

    gitStartBranch=None
    if not gitBranch:
        gitBranch = gitBranchPath.split("/",1)[0]
        gitStartBranch = gitProject.default_branch
        log(f'I Could not find branch from git url {gitBranchPath}. Assuming {gitBranch}.')
    
    gitFilePath = gitBranchPath[len(gitBranch)+1:]
    return gitBranch, gitStartBranch, gitFilePath

def checkIfFileExistsOnBranch(gitBranch, gitFilePath, gitProject):
    gitFilePathExists = False
    gitFileLastUpdate = datetime(MINYEAR, 1, 1, tzinfo=timezone.utc)
    try:
        h = gitProject.files.head(gitFilePath, ref=gitBranch)
        c = gitProject.commits.get(h['X-Gitlab-Last-Commit-Id'])
        gitFileLastUpdate = datetime.fromisoformat(c.committed_date)
        gitFilePathExists = True
    except:
        pass
    return gitFilePathExists,gitFileLastUpdate

def createCommitData(padUrl, padTitle, padRevision, padBody, gitBranch, gitStartBranch, gitFilePath, gitFilePathExists, gitImgActions):
    commitData = {
            'branch': gitBranch,
            'start_branch': gitStartBranch,
            'commit_message': f'Updating pad `{padTitle}` from {padUrl.geturl()} at revision {padRevision["time"]}.',
            'author_name': padRevision['author']['name'],
            'author_email': f'{padRevision["author"]["userid"]}@{padUrl.netloc}',
            'actions': []
        }
    commitData['actions'] += gitImgActions
    commitData['actions'].append({
            'action': 'update' if gitFilePathExists else 'create',
            'file_path': gitFilePath,
            'content': padBody
        })
    return commitData