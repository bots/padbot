import base64
from urllib.parse import urlparse
import mistletoe
import requests
import yaml
from typing import Callable

from stringTools import *

def parseYamlFromMarkdown(description:str, log:Callable[[str],str]=lambda s:s):
    doc = mistletoe.Document(description or "")

    parameters = {}
    for node in doc.children:
        if isinstance(node, mistletoe.block_token.CodeFence):
            try:
                yaml_dict = None
                if node.language == 'yaml':
                    yaml_dict = yaml.safe_load(node.children[0].content)
                if yaml_dict:
                    parameters.update(yaml_dict)
            except yaml.YAMLError as e:
                log(f'E Error parsing YAML: "{e}". Ignoring block.')
    return parameters

def makeLinksRelative(padHistory, gitHistory, iid, padBody, gitUrl, padLinkList, log:Callable[[str],str]=lambda s:s):
    for padLinkUrl in padLinkList:
        if padLinkUrl.geturl() in padHistory and padHistory[padLinkUrl.geturl()] == iid:
            for gitHistoryGitUrl, gitHistoryPadUrl in gitHistory.items():
                if gitHistoryPadUrl == padLinkUrl.geturl():
                    linkLocalPath = relativeUrl(gitHistoryGitUrl, gitUrl.geturl())
                    padBody = padBody.replace(padLinkUrl.geturl(), linkLocalPath)
                    break
        elif padLinkUrl.geturl() in padHistory:
            log(f'I Linked pad at {padLinkUrl} was processed in different issue #{padHistory[padLinkUrl.geturl()]}. Not linking across issues.')
        else:
            log(f'V Linked pad at {padLinkUrl} was not processed. Keeping link to pad.')
    return padBody

def makeImagesRelative(padBody, gitProject, gitBranch, gitFilePath, padImagesList, imagesRelativeFolder:str='uploads', log:Callable[[str],str]=lambda s:s):
    gitImgActions = []
    for imgUrl in padImagesList:
        try:
            imgBasename = imgUrl.path.split('/')[-1]
            imgLocalPath = f'./uploads/{imgBasename}'
            imgGitPath = gitFilePath[:gitFilePath.rfind('/')+1] + f'{imagesRelativeFolder}/{imgBasename}'
            imgContent = requests.get(imgUrl.geturl()).content
            padBody = padBody.replace(imgUrl.geturl(), imgLocalPath)
            try:
                gitProject.files.head(imgGitPath, ref=gitBranch)
            except Exception as e:
                log(f'I Also commiting image {imgBasename} to {imgGitPath}.')
                gitImgActions.append({
                        'action': 'create',
                        'file_path': imgGitPath,
                        'content': base64.b64encode(imgContent).decode(),
                        'encoding': 'base64'
                    })
        except Exception as e:
            log(f'E Could not get linked image {imgUrl.geturl()}: {e}.')
    return padBody, gitImgActions

def getImagesAndLinks(padUrl, padBody):
    padDocument = mistletoe.Document(padBody)
    padDocumentChildren = list(padDocument.children.copy())
    padImagesList = []
    padLinkList = []
    padServer = f'{padUrl.scheme}://{padUrl.netloc}'
    while len(padDocumentChildren) > 0:
        node = padDocumentChildren.pop(0)
        if hasattr(node, 'children'):
            padDocumentChildren = list(node.children) + padDocumentChildren
        if isinstance(node, mistletoe.span_token.Image) and node.src.startswith(padServer):
            padImagesList.append(urlparse(node.src))
        if isinstance(node, mistletoe.span_token.Link) and node.target.startswith(padServer):
            padLinkList.append(urlparse(node.target)._replace(query='', fragment=''))
    return padImagesList,padLinkList