FROM python:alpine

RUN adduser -D app
RUN mkdir -p /app &&\
  chown app:app app &&\
  chmod a+rx app
USER app

WORKDIR /app
COPY . .

RUN pip install -r requirements.txt

CMD python main.py